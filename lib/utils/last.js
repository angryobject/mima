"use strict";

Object.defineProperty(exports, "__esModule", {
   value: true
});
exports.default = last;
function last(arr) {
   return arr[arr.length - 1];
}