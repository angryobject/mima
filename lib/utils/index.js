'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.last = undefined;

var _last = require('./last');

var _last2 = _interopRequireDefault(_last);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.last = _last2.default; /* eslint import/prefer-default-export: 0 */