'use strict';

var _tape = require('tape');

var _tape2 = _interopRequireDefault(_tape);

var _last = require('./last');

var _last2 = _interopRequireDefault(_last);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _tape2.default)('returns last element of array', function (t) {
   t.equals((0, _last2.default)([1, 2, 3]), 3);
   t.equals((0, _last2.default)(['a', 'b', 'c']), 'c');
   t.end();
});