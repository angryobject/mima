#!/usr/bin/env node
'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _fs = require('fs');

var _parser = require('../parser');

var parser = _interopRequireWildcard(_parser);

var _simpleHTMLView = require('../simpleHTMLView');

var _simpleHTMLView2 = _interopRequireDefault(_simpleHTMLView);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function main() {
   var _process$argv = _slicedToArray(process.argv, 4);

   var inputFileName = _process$argv[2];
   var outputFileName = _process$argv[3];


   if (!inputFileName) {
      throw new Error('No input file');
   }

   (0, _simpleHTMLView2.default)(parser.parse((0, _fs.readFileSync)(_path2.default.resolve(inputFileName), 'utf-8'))).then(function (result) {
      if (outputFileName) {
         (0, _fs.writeFileSync)(_path2.default.resolve(outputFileName), result);
      } else {
         /* eslint-disable no-console */
         console.log(result);
         /* eslint-enable */
      }
   });
}

main();