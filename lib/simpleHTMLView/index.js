'use strict';

Object.defineProperty(exports, "__esModule", {
   value: true
});
exports.default = simpleHTMLView;

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _fs = require('fs');

var _posthtml = require('posthtml');

var _posthtml2 = _interopRequireDefault(_posthtml);

var _posthtmlExp = require('posthtml-exp');

var _posthtmlExp2 = _interopRequireDefault(_posthtmlExp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var template = (0, _fs.readFileSync)(_path2.default.resolve(__dirname, 'index.html'), 'utf8');

function treeHTML(tree) {
   var depth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

   return '\n   <div class="Mima-Leaf" data-depth="' + depth + '">\n      <div class="Mima-Leaf-Name">\n         ' + tree.name + '\n         ' + (tree.notes.length ? '<sup>+</sup>' : '') + '\n      </div>\n      ' + (tree.notes.length ? '<div class="Mima-Leaf-Notes">\n            ' + tree.notes.map(function (note) {
      return '\n                  <div class="Mima-Leaf-Note">\n                     <p>' + note + '</p>\n                  </div>\n               ';
   }) + '\n         </div>' : '') + '\n      <div class="Mima-Leaf-Children">\n         ' + tree.children.map(function (child) {
      return treeHTML(child, depth + 1);
   }).join('') + '\n      </div>\n   </div>';
}

function simpleHTMLView(tree) {
   return (0, _posthtml2.default)([(0, _posthtmlExp2.default)({ locals: {
         title: tree.name,
         tree: treeHTML(tree)
      } })]).process(template)
   /* eslint-disable no-console */
   .then(function (result) {
      return result.html;
   }, console.log);
   /* eslint-enable */
}