'use strict';

Object.defineProperty(exports, "__esModule", {
   value: true
});
exports.NODE_TYPE_NOTE = exports.NODE_TYPE_BREAK = exports.NODE_TYPE_LEAF = undefined;
exports.default = createNodes;

var _utils = require('../utils');

var NODE_TYPE_LEAF = exports.NODE_TYPE_LEAF = 'LEAF';

var NODE_TYPE_BREAK = exports.NODE_TYPE_BREAK = 'BREAK';
var NODE_TYPE_NOTE = exports.NODE_TYPE_NOTE = 'NOTE';

function indentationError(index) {
   return new Error('Check indentation at line ' + (index + 1));
}

function getIndentation(line) {
   var whitespace = line.match(/^\s+/);
   return whitespace == null ? 0 : whitespace[0].length;
}

function createNode(line, depth) {
   var text = line.trim();
   var controlChar = text.charAt(0);

   var type = void 0;
   var content = void 0;

   switch (controlChar) {
      case '\\':
         type = NODE_TYPE_BREAK;
         content = text.slice(1).trim();
         break;
      case '+':
         type = NODE_TYPE_NOTE;
         content = text.slice(1).trim();
         break;
      default:
         type = NODE_TYPE_LEAF;
         content = text;
   }

   return {
      type: type,
      content: content,
      depth: depth
   };
}

function createNodes(str) {
   var lines = str.split('\n');
   var offset = getIndentation(lines[0]);
   var indent = offset || getIndentation(lines[1]);

   var checkNodeDepth = function checkNodeDepth(node, lastDepth, index) {
      var type = node.type;
      var depth = node.depth;

      var base = depth % 1 !== 0;
      var leafNode = type === NODE_TYPE_LEAF && depth - lastDepth > 1;
      var breakNode = type === NODE_TYPE_BREAK && depth !== lastDepth;
      var noteNode = type === NODE_TYPE_NOTE && depth !== lastDepth;

      if (base || leafNode || breakNode || noteNode) {
         throw indentationError(index);
      }
   };

   return lines.reduce(function (acc, line, index) {
      if (!line.trim()) {
         return acc;
      }

      var depth = (getIndentation(line) - offset) / indent;
      var node = createNode(line, depth);

      checkNodeDepth(node, acc.depth, index);

      if (node.type === NODE_TYPE_BREAK) {
         (0, _utils.last)(acc.nodes).content += ' ' + node.content;
      } else {
         acc.depth = node.depth = depth;
         acc.nodes.push(node);
      }

      return acc;
   }, { depth: 0, nodes: [] }).nodes;
}