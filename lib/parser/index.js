'use strict';

Object.defineProperty(exports, "__esModule", {
   value: true
});
exports.parse = parse;

var _createNodes = require('./createNodes');

var _createNodes2 = _interopRequireDefault(_createNodes);

var _createTree = require('./createTree');

var _createTree2 = _interopRequireDefault(_createTree);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint import/prefer-default-export: 0*/
function parse(str) {
   return (0, _createTree2.default)((0, _createNodes2.default)(str));
}
/* eslint-enable */