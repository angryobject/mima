'use strict';

var _tape = require('tape');

var _tape2 = _interopRequireDefault(_tape);

var _index = require('./index');

var parser = _interopRequireWildcard(_index);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function removeParent(root) {
   /* eslint-disable no-param-reassign */
   delete root.parent;
   root.children.forEach(removeParent);
   /* eslint-enable */

   return root;
}

var treeStr = 'Root\n   Leaf1\n      Leaf3\n      \\ leaf3 continue\n      + leaf 3 note\n   Leaf2\n   + leaf 2 note\n   \\ leaf 2 note continue\n      Leaf4\n         Leaf5\n   Leaf6';

var treeStrExtraIndentation = '   Root\n      Leaf1\n         Leaf3\n         \\ leaf3 continue\n         + leaf 3 note\n      Leaf2\n      + leaf 2 note\n      \\ leaf 2 note continue\n         Leaf4\n            Leaf5\n      Leaf6';

var treeObj = {
   name: 'Root',
   children: [{
      name: 'Leaf1',
      children: [{
         name: 'Leaf3 leaf3 continue',
         children: [],
         notes: ['leaf 3 note']
      }],
      notes: []
   }, {
      name: 'Leaf2',
      children: [{
         name: 'Leaf4',
         children: [{
            name: 'Leaf5',
            children: [],
            notes: []
         }],
         notes: []
      }],
      notes: ['leaf 2 note leaf 2 note continue']
   }, {
      name: 'Leaf6',
      children: [],
      notes: []
   }],
   notes: []
};

(0, _tape2.default)('parse', function (t) {
   // test without recursive "parent" filed
   t.deepEquals(removeParent(parser.parse(treeStr)), treeObj, 'test without recursive "parent" filed');

   // test parents explicitly
   t.equal(parser.parse(treeStr).parent, null, 'root parent should be null');
   (function testParent(root) {
      root.children.forEach(function (child) {
         t.equal(child.parent, root, 'test parent explicitly');
         testParent(child);
      });
   })(parser.parse(treeStr));

   t.end();
});

(0, _tape2.default)('parse with offset', function (t) {
   t.deepEquals(removeParent(parser.parse(treeStrExtraIndentation)), treeObj, 'skip parents');

   t.end();
});