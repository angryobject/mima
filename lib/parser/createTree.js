'use strict';

Object.defineProperty(exports, "__esModule", {
   value: true
});

var _createNodes = require('./createNodes');

function createLeaf(node) {
   return {
      name: node.content,
      parent: null,
      children: [],
      notes: []
   };
}

function createTree(nodes) {
   var lastNode = nodes[0];
   var lastLeaf = createLeaf(lastNode);

   var leafs = [lastLeaf];
   var parents = [];

   nodes.forEach(function (node, index) {
      if (index === 0) {
         return;
      }

      if (node.depth > lastNode.depth) {
         parents.push(lastLeaf);
      } else if (node.depth < lastNode.depth) {
         parents.splice(node.depth);
      }

      if (node.type === _createNodes.NODE_TYPE_NOTE) {
         lastLeaf.notes.push(node.content);
      } else {
         lastNode = node;
         lastLeaf = createLeaf(node);

         lastLeaf.parent = parents[node.depth - 1];
         parents[node.depth - 1].children.push(lastLeaf);
         leafs.push(lastLeaf);
      }
   });

   return parents[0];
}

exports.default = createTree;