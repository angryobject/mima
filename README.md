CLI util to convert this:

```
Root
   Leaf 1
      Leaf 1-1
      Leaf 1-2
         Leaf 1-2-1
         Leaf 1-2-2
      Leaf 1-3
         Leaf 1-3-4
   Leaf 2
      Leaf 2-1
      Leaf 2-2
   Leaf 3
   + plus sign denotes a note that will show up on hover,
   \ use backslash to wrap previous line and continue typing
   Leaf
   \ 4
```

to this:

![mima resulting html](./example/example.png)

See `example` folder.
