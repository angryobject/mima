/* @flow */

export default function last(arr: any[]) {
   return arr[arr.length - 1];
}
