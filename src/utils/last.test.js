import test from 'tape';
import last from './last';

test('returns last element of array', (t) => {
   t.equals(last([1, 2, 3]), 3);
   t.equals(last(['a', 'b', 'c']), 'c');
   t.end();
});
