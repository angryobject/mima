#!/usr/bin/env node

import path from 'path';
import { readFileSync, writeFileSync } from 'fs';
import * as parser from '../parser';
import simpleHTMLView from '../simpleHTMLView';

function main() {
   const [,, inputFileName, outputFileName] = process.argv;

   if (!inputFileName) {
      throw new Error('No input file');
   }

   simpleHTMLView(parser.parse(
      readFileSync(path.resolve(inputFileName), 'utf-8')
   )).then((result) => {
      if (outputFileName) {
         writeFileSync(path.resolve(outputFileName), result);
      } else {
         /* eslint-disable no-console */
         console.log(result);
         /* eslint-enable */
      }
   });
}


main();
