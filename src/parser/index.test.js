import test from 'tape';
import * as parser from './index';

function removeParent(root) {
   /* eslint-disable no-param-reassign */
   delete root.parent;
   root.children.forEach(removeParent);
   /* eslint-enable */

   return root;
}

const treeStr =
`Root
   Leaf1
      Leaf3
      \\ leaf3 continue
      + leaf 3 note
   Leaf2
   + leaf 2 note
   \\ leaf 2 note continue
      Leaf4
         Leaf5
   Leaf6`;

const treeStrExtraIndentation =
`   Root
      Leaf1
         Leaf3
         \\ leaf3 continue
         + leaf 3 note
      Leaf2
      + leaf 2 note
      \\ leaf 2 note continue
         Leaf4
            Leaf5
      Leaf6`;

const treeObj = {
   name: 'Root',
   children: [
      {
         name: 'Leaf1',
         children: [{
            name: 'Leaf3 leaf3 continue',
            children: [],
            notes: ['leaf 3 note'],
         }],
         notes: [],
      },
      {
         name: 'Leaf2',
         children: [{
            name: 'Leaf4',
            children: [{
               name: 'Leaf5',
               children: [],
               notes: [],
            }],
            notes: [],
         }],
         notes: ['leaf 2 note leaf 2 note continue'],
      },
      {
         name: 'Leaf6',
         children: [],
         notes: [],
      },
   ],
   notes: [],
};

test('parse', (t) => {
   // test without recursive "parent" filed
   t.deepEquals(removeParent(parser.parse(treeStr)), treeObj, 'test without recursive "parent" filed');

   // test parents explicitly
   t.equal(parser.parse(treeStr).parent, null, 'root parent should be null');
   (function testParent(root) {
      root.children.forEach((child) => {
         t.equal(child.parent, root, 'test parent explicitly');
         testParent(child);
      });
   }(parser.parse(treeStr)));

   t.end();
});

test('parse with offset', (t) => {
   t.deepEquals(
      removeParent(parser.parse(treeStrExtraIndentation)),
      treeObj,
      'skip parents'
   );

   t.end();
});
