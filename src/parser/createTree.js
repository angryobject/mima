/* @flow */

import type { Node } from './createNodes';
import { NODE_TYPE_NOTE } from './createNodes';

export type Leaf = {
   name: string,
   parent: ?Leaf,
   children: Leaf[],
   notes: string[],
}

function createLeaf(node: Node): Leaf {
   return {
      name: node.content,
      parent: null,
      children: [],
      notes: [],
   };
}

function createTree(nodes: Node[]): Leaf {
   let lastNode = nodes[0];
   let lastLeaf = createLeaf(lastNode);

   const leafs = [lastLeaf];
   const parents = [];

   nodes.forEach((node, index) => {
      if (index === 0) { return; }

      if (node.depth > lastNode.depth) {
         parents.push(lastLeaf);
      } else if (node.depth < lastNode.depth) {
         parents.splice(node.depth);
      }

      if (node.type === NODE_TYPE_NOTE) {
         lastLeaf.notes.push(node.content);
      } else {
         lastNode = node;
         lastLeaf = createLeaf(node);

         lastLeaf.parent = parents[node.depth - 1];
         parents[node.depth - 1].children.push(lastLeaf);
         leafs.push(lastLeaf);
      }
   });

   return parents[0];
}

export default createTree;
