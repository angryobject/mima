/* @flow */

import createNodes from './createNodes';
import type { Leaf } from './createTree';
import createTree from './createTree';

/* eslint import/prefer-default-export: 0*/
export function parse(str: string): Leaf {
   return createTree(createNodes(str));
}
/* eslint-enable */
