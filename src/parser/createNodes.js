/* @flow */

import { last } from '../utils';

export type Node = {
   type: string,
   content: string,
   depth: number,
};

export const NODE_TYPE_LEAF = 'LEAF';
export const NODE_TYPE_BREAK = 'BREAK';
export const NODE_TYPE_NOTE = 'NOTE';

function indentationError(index: number) {
   return new Error(`Check indentation at line ${index + 1}`);
}

function getIndentation(line: string) {
   const whitespace = line.match((/^\s+/));
   return whitespace == null ? 0 : whitespace[0].length;
}

function createNode(line: string, depth: number): Node {
   const text = line.trim();
   const controlChar = text.charAt(0);

   let type: string;
   let content: string;

   switch (controlChar) {
      case '\\':
         type = NODE_TYPE_BREAK;
         content = text.slice(1).trim();
         break;
      case '+':
         type = NODE_TYPE_NOTE;
         content = text.slice(1).trim();
         break;
      default:
         type = NODE_TYPE_LEAF;
         content = text;
   }

   return {
      type,
      content,
      depth,
   };
}

export default function createNodes(str: string): Node[] {
   const lines = str.split('\n');
   const offset = getIndentation(lines[0]);
   const indent = offset || getIndentation(lines[1]);

   const checkNodeDepth = (node: Node, lastDepth: number, index: number) => {
      const { type, depth } = node;
      const base = depth % 1 !== 0;
      const leafNode = type === NODE_TYPE_LEAF && depth - lastDepth > 1;
      const breakNode = type === NODE_TYPE_BREAK && depth !== lastDepth;
      const noteNode = type === NODE_TYPE_NOTE && depth !== lastDepth;

      if (base || leafNode || breakNode || noteNode) {
         throw indentationError(index);
      }
   };

   return lines.reduce((acc, line, index) => {
      if (!line.trim()) { return acc; }

      const depth = (getIndentation(line) - offset) / indent;
      const node = createNode(line, depth);

      checkNodeDepth(node, acc.depth, index);

      if (node.type === NODE_TYPE_BREAK) {
         last(acc.nodes).content += ` ${node.content}`;
      } else {
         acc.depth = node.depth = depth;
         acc.nodes.push(node);
      }

      return acc;
   }, { depth: 0, nodes: [] }).nodes;
}
