import path from 'path';
import { readFileSync } from 'fs';
import posthtml from 'posthtml';
import exp from 'posthtml-exp';
import type { Leaf } from '../parser/createTree';

const template: string = readFileSync(path.resolve(__dirname, 'index.html'), 'utf8');

function treeHTML(tree: Leaf[], depth: number = 0): string {
   return `
   <div class="Mima-Leaf" data-depth="${depth}">
      <div class="Mima-Leaf-Name">
         ${tree.name}
         ${tree.notes.length ? '<sup>+</sup>' : ''}
      </div>
      ${tree.notes.length ?
         `<div class="Mima-Leaf-Notes">
            ${tree.notes.map(note =>
               `
                  <div class="Mima-Leaf-Note">
                     <p>${note}</p>
                  </div>
               `
            )}
         </div>` : ''
      }
      <div class="Mima-Leaf-Children">
         ${tree.children.map(child => treeHTML(child, depth + 1)).join('')}
      </div>
   </div>`;
}

export default function simpleHTMLView(tree: Leaf[]): string {
   return posthtml([exp({ locals: {
      title: tree.name,
      tree: treeHTML(tree),
   } })])
      .process(template)
      /* eslint-disable no-console */
      .then(result => result.html, console.log);
      /* eslint-enable */
}
